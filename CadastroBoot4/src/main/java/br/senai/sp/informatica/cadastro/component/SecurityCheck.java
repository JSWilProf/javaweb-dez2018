package br.senai.sp.informatica.cadastro.component;

public interface SecurityCheck {
	boolean isInRole(String role);
	String getUserName();
}
