package br.senai.sp.informatica.cadastro.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.senai.sp.informatica.cadastro.model.Servico;

public interface ServicoRepository extends JpaRepository<Servico, Integer>{

}
