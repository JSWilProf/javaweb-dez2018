package br.senai.sp.informatica.cadastro.service;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.senai.sp.informatica.cadastro.model.Cliente;
import br.senai.sp.informatica.cadastro.model.ListaDeClientes;
import br.senai.sp.informatica.cadastro.repository.ClienteRepository;

@Service
public class ClienteService {
	@Autowired
	private ClienteRepository clienteRepo;
		
	public void salvar(Cliente obj) {
		clienteRepo.save(obj);
	}
	
	public ListaDeClientes getClientes() {
		return ListaDeClientes.builder()
				.clientes(clienteRepo.findAll().stream()
							.filter(cli -> !cli.isDesativado())
							.collect(Collectors.toList())
				)
				.build();
	}
	
	public void removeCliente(ListaDeClientes lista) {
		Arrays.stream(lista.getIds())
		.forEach(id -> {
			Cliente cliente = clienteRepo.findById(id).orElse(null);
			if(cliente != null) {
				cliente.setDesativado(true);
				clienteRepo.save(cliente);
			}
		});
	}
	
	public Cliente getCliente(int id) {
		return clienteRepo.findById(id).orElse(null);
	}
}
