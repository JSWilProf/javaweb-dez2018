package br.senai.sp.informatica.cadastro.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.senai.sp.informatica.cadastro.model.Cliente;
import br.senai.sp.informatica.cadastro.repository.ClienteRepository;

@Controller
public class CadastroController {
	private ClienteRepository clienteDao = ClienteRepository.repo;
	
	@RequestMapping({"/", "index.html"})
	public String inicio() {
		return "index";
	}

	@RequestMapping({ "/editaCliente" })
	public ModelAndView editaCliente() {
		return new ModelAndView("editaCliente", "cliente", new Cliente());
	}
	
	@RequestMapping("/cadastra")
	public ModelAndView cadastra(@ModelAttribute("cliente") Cliente cliente, RedirectAttributes redirectAttributes) {
		clienteDao.salvar(cliente);

		redirectAttributes.addFlashAttribute("mensagem", "Cliente cadastrado");
		redirectAttributes.addFlashAttribute("tipo.mensagem", "alert-success");
		return new ModelAndView("redirect:/editaCliente");
	}

	@RequestMapping("/listaCliente")
	public ModelAndView listaCliente() {
		return new ModelAndView("listaCliente", "cadastro", clienteDao.getClientes());
	}

}
