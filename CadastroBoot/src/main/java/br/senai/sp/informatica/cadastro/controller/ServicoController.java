package br.senai.sp.informatica.cadastro.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.senai.sp.informatica.cadastro.model.Servico;
import br.senai.sp.informatica.cadastro.service.ServicoService;

@Controller
public class ServicoController {
	@Autowired
	private ServicoService servicoDao;
	
	@RequestMapping("/editaServico")
	public ModelAndView editaServico() {
		return new ModelAndView("editaServico", "servico", new Servico());
	}
	
	@RequestMapping("/salvaServico")
	public ModelAndView salvaServico(@ModelAttribute("servico") @Valid Servico servico, BindingResult result,
			RedirectAttributes attributes) {
		
		if(result.hasErrors()) {
			return new ModelAndView("editaServico", "servico", servico);
		} else {
			servicoDao.salvar(servico);
			attributes.addFlashAttribute("mensagem", "Serviço cadastrado");
			attributes.addFlashAttribute("tipo.mensagem", "alert-success");
			return new ModelAndView("redirect:/editaServico");
		}
	}

	@RequestMapping("/listaServico")
	public ModelAndView listaServico() {
		ModelAndView model = new ModelAndView("listaServico", "servicos", servicoDao.getServicos());
		return model;
	}

	@RequestMapping("/removeServico/{idServico}")
	public String removeUsuario(@PathVariable("idServico") int id, Model model, 
			RedirectAttributes redirectAttributes) {

	    servicoDao.removeServico(id);
		redirectAttributes.addFlashAttribute("mensagem", "Serviço excluído");
		redirectAttributes.addFlashAttribute("tipo.mensagem", "alert-success");
		return "redirect:/listaServico";
	}

}
