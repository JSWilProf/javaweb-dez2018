package br.senai.sp.informatica.cadastro.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.senai.sp.informatica.cadastro.model.Cliente;
import br.senai.sp.informatica.cadastro.model.ListaDeClientes;
import br.senai.sp.informatica.cadastro.model.ListaDeServicos;
import br.senai.sp.informatica.cadastro.model.Servico;
import br.senai.sp.informatica.cadastro.service.ClienteService;
import br.senai.sp.informatica.cadastro.service.ServicoService;

@Controller
public class CadastroController {
	@Autowired
	private ClienteService clienteDao;
	@Autowired
	private ServicoService servicoDao;
	
	@RequestMapping({"/", "index.html"})
	public String inicio() {
		return "index";
	}

	@RequestMapping({ "/editaCliente" })
	public ModelAndView editaCliente() {
		return new ModelAndView("editaCliente", "cliente", new Cliente());
	}
	
	//TODO: Implementa validação
	@RequestMapping("/cadastra")
	public ModelAndView cadastra(@ModelAttribute("cliente") @Valid Cliente cliente, BindingResult result, 
			RedirectAttributes redirectAttributes) {
		
		if (result.hasErrors()) {
			return new ModelAndView("editaCliente", "cliente", cliente);
		} else {
			Integer id = cliente.getIdCliente();
			clienteDao.salvar(cliente);
	
			if (id == null) {
				redirectAttributes.addFlashAttribute("mensagem", "Cliente cadastrado");
				redirectAttributes.addFlashAttribute("tipo.mensagem", "alert-success");
				return new ModelAndView("redirect:/editaCliente");
			} else {
				redirectAttributes.addFlashAttribute("mensagem", "Cliente atualizado");
				redirectAttributes.addFlashAttribute("tipo.mensagem", "alert-success");
				return new ModelAndView("redirect:/listaCliente");
			}
		}
	}

	//TODO: Integra Serviços do Cliente
	@RequestMapping("/listaCliente")
	public ModelAndView listaCliente() {
		ModelAndView model = new ModelAndView("listaCliente", "cadastro", clienteDao.getClientes());
		model.addObject("listaservicos", servicoDao.getListaDeServicos());
		return model;
	}

	@RequestMapping("/removeCliente")
	public String removeCliente(Model model, @ModelAttribute("cadastro") ListaDeClientes lista,
			RedirectAttributes redirectAttributes) {
		if(lista.getIds().length == 0) {
			model.addAttribute("mensagem", "Nenhum Cliente foi selecionado");
			model.addAttribute("tipo.mensagem", "alert-warning");
			return "listaCliente";
		} else if(clienteDao.removeCliente(lista)) {
			redirectAttributes.addAttribute("mensagem", "Os Cliente(s) foram excluído(s)");
			redirectAttributes.addAttribute("tipo.mensagem", "alert-success");
			return "redirect:/listaCliente";
		} else {
			model.addAttribute("mensagem", "Houve falha ao excluir o(s) Cliente(s)");
			model.addAttribute("tipo.mensagem", "alert-danger");
			return "listaCliente";
		}	
	}
	
	@RequestMapping("/editaCliente/{clienteId}")
	public ModelAndView editaCliente(@PathVariable("clienteId") int id) {
		ModelAndView model;
		
		Cliente cliente = clienteDao.getCliente(id);
		
		if(cliente != null) {
			model = new ModelAndView("editaCliente", "cliente", cliente);
			model.addObject("editar", true);
		} else {
			model = new ModelAndView("listaDeCliente", "cadastro", clienteDao.getClientes());
			model.addObject("mensagem", "Houve falha ao localizar o Cliente");
			model.addObject("tipo.mensagem", "alert-danger");
		}
		
		return model;
	}

	//TODO: Gerencia Serviços do Cliente
	@RequestMapping("/carregaServicos/{clienteId}")
	@ResponseBody
	public List<Servico> carregaServicos(@PathVariable("clienteId") int id,
			RedirectAttributes redirectAttributes) {
		Cliente cliente = clienteDao.getCliente(id);
		
		if(cliente != null) {
			return servicoDao.getServicos(cliente);
		} else {
			return new ArrayList<>();			
		}
	}
	
	//TODO: Gerencia Serviços do Cliente
	@RequestMapping("/selecionaServico")
	public ModelAndView selecionaServico(@ModelAttribute("listaservicos") ListaDeServicos lista,
			RedirectAttributes redirectAttributes) {
		ModelAndView model;

		// Localiza o Cliente pelo ID
		Cliente cliente = clienteDao.getCliente(lista.getIdCliente());

		// Se o Cliente foi encontrado
		if (cliente != null) {
			// Garanta que a lista de serviços não seja Nula
			if (cliente.getServicos() == null)
				cliente.setServicos(new ArrayList<>());

			// Cria a lista com os IDs de serviços a serem excluidos do cliente
			List<Integer> aDeletar = cliente.getServicos().stream().map(Servico::getIdServico)
					.filter(id -> Arrays.binarySearch(lista.getIds(), id) < 0).collect(Collectors.toList());

			// Exclua os IDs de serviço do cliente
			aDeletar.stream().forEach(id -> cliente.getServicos().removeIf(svc -> svc.getIdServico() == id));

			// Cria a lista dos servicos a serem incluidos para o cliente
			List<Servico> aIncluir = Arrays.stream(lista.getIds()).boxed().map(id -> servicoDao.getServico(id))
					.filter(srv -> !cliente.getServicos().contains(srv)).collect(Collectors.toList());

			aIncluir.stream().forEach(svc -> cliente.getServicos().add(svc));

			clienteDao.salvar(cliente);
			redirectAttributes.addFlashAttribute("mensagem", "O(s) Serviço(s) foram atualizados");
			redirectAttributes.addFlashAttribute("tipo.mensagem", "alert-success");
			model = new ModelAndView("redirect:/listaCliente");

			return model;
		} else {
			model = new ModelAndView("listaCliente", "cadastro", clienteDao.getClientes());
			model.addObject("mensagem", "O Cliente não foi encontrado, os serviços não foram salvos");
			model.addObject("tipo.mensagem", "alert-danger");
			return model;
		}
	}
}
