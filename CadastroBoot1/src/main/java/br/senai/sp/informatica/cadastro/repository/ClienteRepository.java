package br.senai.sp.informatica.cadastro.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import br.senai.sp.informatica.cadastro.model.Cliente;
import br.senai.sp.informatica.cadastro.model.ListaDeClientes;

public class ClienteRepository {
	public static ClienteRepository repo = new ClienteRepository();
	
	private List<Cliente> cadastro = new ArrayList<>();
	private int id = 0;
	
	private ClienteRepository() {}
	
	public void salvar(Cliente obj) {
		Optional<Cliente> cliente = cadastro.stream()
			.filter(cli -> cli.getNome().equals(obj.getNome()))
			.findAny();
		if(!cliente.isPresent()) {
			obj.setIdCliente(id++);
			cadastro.add(obj);
		}
	}
	
	public ListaDeClientes getClientes() {
		return ListaDeClientes.builder()
				.clientes(cadastro.stream()
							.map(Cliente::clone)
							.collect(Collectors.toList()))
				.build();
	}
}
