package br.senai.sp.informatica.cadastro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.senai.sp.informatica.cadastro.model.Cliente;
import br.senai.sp.informatica.cadastro.model.ListaDeClientes;
import br.senai.sp.informatica.cadastro.service.ClienteService;

@Controller
public class CadastroController {
	@Autowired
	private ClienteService clienteDao;
	
	@RequestMapping({"/", "index.html"})
	public String inicio() {
		return "index";
	}

	@RequestMapping({ "/editaCliente" })
	public ModelAndView editaCliente() {
		return new ModelAndView("editaCliente", "cliente", new Cliente());
	}
	
	@RequestMapping("/cadastra")
	public ModelAndView cadastra(@ModelAttribute("cliente") Cliente cliente, RedirectAttributes redirectAttributes) {
		Integer id = cliente.getIdCliente();
		clienteDao.salvar(cliente);

		if (id == null) {
			redirectAttributes.addFlashAttribute("mensagem", "Cliente cadastrado");
			redirectAttributes.addFlashAttribute("tipo.mensagem", "alert-success");
			return new ModelAndView("redirect:/editaCliente");
		} else {
			redirectAttributes.addFlashAttribute("mensagem", "Cliente atualizado");
			redirectAttributes.addFlashAttribute("tipo.mensagem", "alert-success");
			return new ModelAndView("redirect:/listaCliente");
		}
	}

	@RequestMapping("/listaCliente")
	public ModelAndView listaCliente() {
		return new ModelAndView("listaCliente", "cadastro", clienteDao.getClientes());
	}

	@RequestMapping("/removeCliente")
	public String removeCliente(Model model, @ModelAttribute("cadastro") ListaDeClientes lista,
			RedirectAttributes redirectAttributes) {
		if(lista.getIds().length == 0) {
			model.addAttribute("mensagem", "Nenhum Cliente foi selecionado");
			model.addAttribute("tipo.mensagem", "alert-warning");
			return "listaCliente";
		} else if(clienteDao.removeCliente(lista)) {
			redirectAttributes.addAttribute("mensagem", "Os Cliente(s) foram excluído(s)");
			redirectAttributes.addAttribute("tipo.mensagem", "alert-success");
			return "redirect:/listaCliente";
		} else {
			model.addAttribute("mensagem", "Houve falha ao excluir o(s) Cliente(s)");
			model.addAttribute("tipo.mensagem", "alert-danger");
			return "listaCliente";
		}	
	}
	
	@RequestMapping("/editaCliente/{clienteId}")
	public ModelAndView editaCliente(@PathVariable("clienteId") int id) {
		ModelAndView model;
		
		Cliente cliente = clienteDao.getCliente(id);
		
		if(cliente != null) {
			model = new ModelAndView("editaCliente", "cliente", cliente);
			model.addObject("editar", true);
		} else {
			model = new ModelAndView("listaDeCliente", "cadastro", clienteDao.getClientes());
			model.addObject("mensagem", "Houve falha ao localizar o Cliente");
			model.addObject("tipo.mensagem", "alert-danger");
		}
		
		return model;
	}
}
