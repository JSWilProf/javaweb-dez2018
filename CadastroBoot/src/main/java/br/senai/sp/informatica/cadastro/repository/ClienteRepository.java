package br.senai.sp.informatica.cadastro.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.senai.sp.informatica.cadastro.model.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Integer> {
}