package br.senai.sp.informatica.cadastro.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.senai.sp.informatica.cadastro.model.Cliente;
import br.senai.sp.informatica.cadastro.model.ListaDeServicos;
import br.senai.sp.informatica.cadastro.model.Servico;
import br.senai.sp.informatica.cadastro.repository.ServicoRepository;

@Service
public class ServicoService {
	@Autowired
	private ServicoRepository servicoRepo;
	
	public void salvar(Servico obj) {
		servicoRepo.save(obj);
	}
	
	public ListaDeServicos getListaDeServicos() {
		return new ListaDeServicos(getServicos(), null, -1);
	}
	
	public List<Servico> getServicos() {
		return servicoRepo.findAll().stream()
				.filter(serv -> !serv.isDesativado())
				.collect(Collectors.toList());
	}
	
	public List<Servico> getServicos(Cliente cliente) {
		return servicoRepo.findAll().stream()
				.filter(servico -> !servico.isDesativado())
				.map(servico -> {
					servico.setSelecionado(cliente.getServicos().contains(servico));
					return servico;
				}).collect(Collectors.toList());
	}
	
	public void removeServico(int id) {
		Servico servico = getServico(id);
		if(servico != null) {
			servico.setDesativado(true);
			servicoRepo.save(servico);
		}
	}
	
	public Servico getServico(int id) {
		return servicoRepo.findById(id).orElse(null);
	}
}
