package br.senai.sp.informatica;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Mensagem {
	@Builder.Default
	private String titulo = "";
	@Builder.Default
	private String texto = "";
	@Builder.Default
	private String url = "";
}