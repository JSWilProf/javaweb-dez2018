package br.senai.sp.informatica.cadastro.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.senai.sp.informatica.cadastro.model.Cliente;
import br.senai.sp.informatica.cadastro.model.ListaDeClientes;
import br.senai.sp.informatica.cadastro.model.ListaDeServicos;
import br.senai.sp.informatica.cadastro.model.Servico;
import br.senai.sp.informatica.cadastro.service.ClienteService;
import br.senai.sp.informatica.cadastro.service.ServicoService;

@Controller
public class CadastroController {
	@Autowired
	private ClienteService clienteDao;
	@Autowired
	private ServicoService servicoDao;
	
	@RequestMapping({"/", "index.html"})
	public String inicio() {
		return "index";
	}
	
	@RequestMapping("/editaCliente")
	public ModelAndView editaCliente() {
		return new ModelAndView("editaCliente", "cliente", new Cliente());
	}
	
	@RequestMapping("/cadastrar")
	public ModelAndView cadastar(@ModelAttribute("cliente") @Valid Cliente cliente, BindingResult result,
			RedirectAttributes attributes) {
		
		if(result.hasErrors()) {
			return new ModelAndView("editaCliente", "cliente", cliente);
		} else {
			Integer id = cliente.getIdCliente();
			clienteDao.salvar(cliente);
			
			if(id != null) {
				attributes.addFlashAttribute("mensagem", "Dados de Cliente alterado");
				attributes.addFlashAttribute("tipo.mensagem", "alert-success");
				return new ModelAndView("redirect:/listaCliente");			
			} else {
				attributes.addFlashAttribute("mensagem", "Cliente cadastrado");
				attributes.addFlashAttribute("tipo.mensagem", "alert-success");
				return new ModelAndView("redirect:/editaCliente");
			}
		}
	}
	
	@RequestMapping("/listaCliente")
	public ModelAndView listaCliente() {
		ModelAndView model = new ModelAndView("listaCliente", "cadastro", clienteDao.getClientes());
		model.addObject("listaServicos", servicoDao.getListaDeServicos());
		return model;
	}

	@RequestMapping("/removeCliente")
	public String removeCliente(Model model, @ModelAttribute("cadastro") ListaDeClientes lista,
			RedirectAttributes attributes) {
		if(lista.getIds().length == 0) {
			model.addAttribute("mensagem", "Nenhum Cliente foi selecionado");
			model.addAttribute("tipo.mensagem", "alert-warning");
			return "listaCliente";
		} else {
			clienteDao.removeCliente(lista);
			attributes.addAttribute("mensagem", "Os Clientes foram excluídos");
			attributes.addAttribute("tipo.mensagem", "alert-success");
			return "redirect:/listaCliente";
		}
	}
	
	@RequestMapping("/editaCliente/{clienteId}")
	public ModelAndView editaCliente(@PathVariable("clienteId") int id) {
		ModelAndView model;
		
		Cliente cliente = clienteDao.getCliente(id);
		
		if(cliente != null) {
			model = new ModelAndView("editaCliente", "cliente", cliente);
			model.addObject("editar", true);
		} else {
			model = new ModelAndView("listaCliente", "cadastro", clienteDao.getClientes());
			model.addObject("mensagem", "Houve falha ao localizar o Cliente");
			model.addObject("tipo.mensagem", "alert-danger");
		}
		return model;
	}

	@ResponseBody
	@RequestMapping("/carregaServicos/{clienteId}")
	public List<Servico> carregaServicos(@PathVariable("clienteId") int id) {
		Cliente cliente = clienteDao.getCliente(id);
		
		if(cliente != null) {
			return servicoDao.getServicos(cliente);
		} else {
			return new ArrayList<>();
		}
	}

	@RequestMapping("/selecionaServico")
	public ModelAndView selecionaServico(@ModelAttribute("listaServicos") ListaDeServicos lista, 
			RedirectAttributes attributes) {
		ModelAndView model;
		
		Cliente cliente = clienteDao.getCliente(lista.getIdCliente());
		
		if(cliente != null) {
			// Garante que a lista de serviços não seja nula
			if(cliente.getServicos() == null) {
				cliente.setServicos(new ArrayList<>());
			}
			
			// Criar uma lista com os serviços a serem excluidos
			List<Integer> aDeletar = cliente.getServicos().stream()
					.map(Servico::getIdServico)
					.filter(id -> Arrays.binarySearch(lista.getIds(), id) < 0)
					.collect(Collectors.toList());
			
			// Remove os serviços do Cliente
			aDeletar.stream()
				.forEach(id -> cliente.getServicos()
						.removeIf(srv -> srv.getIdServico() == id));
			
			// Criar uma lista com os serviços a serem incluidos
			List<Servico> aIncluir = Arrays.stream(lista.getIds())
					.boxed()
					.map(id -> servicoDao.getServico(id))
					.filter(srv -> !cliente.getServicos().contains(srv))
					.collect(Collectors.toList());
			
			// Adiciona os novos Serviços
			aIncluir.stream()
				.forEach(srv -> cliente.getServicos().add(srv));
			
			clienteDao.salvar(cliente);
			
			attributes.addFlashAttribute("mensagem", "Os Serviços foram atualizados");
			attributes.addFlashAttribute("tipo.mensagem", "alert-success");
			return new ModelAndView("redirect:/listaCliente");
		} else {
			model = new ModelAndView("listaCliente", "cadastro", clienteDao.getClientes());
			model.addObject("mensagem", "O Cliente não foi encontrado, os serviços não foram salvos");
			model.addObject("tipo.mensagem", "alert-danger");
			return model;
		}
	}
	
}












