<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	
	<meta http-equiv="cache-control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="expires" content="Mon, 17 jul 2000 10:10:00 GMT" />
	
	<title>Dados Cadastrais</title>
	<link rel="stylesheet" type="text/css" href="style/style.css">
</head>
<body>
	<div id="wrapper">
		<header role="banner" class="group">
			<div id="logo"><a href="index.html">Dados Cadastrais</a></div>
		</header>

		<div id="content" role="main">
			<div id="form1">
				<fieldset>
					<h2><c:out value="${msg.titulo}" /></h2>
					<p><c:out value="${msg.texto}" /></p>
				</fieldset>
			</div>
		</div>

	</div>
</body>
</html>