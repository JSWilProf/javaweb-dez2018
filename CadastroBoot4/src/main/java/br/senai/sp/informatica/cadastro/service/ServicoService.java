package br.senai.sp.informatica.cadastro.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.senai.sp.informatica.cadastro.model.Cliente;
import br.senai.sp.informatica.cadastro.model.ListaDeServicos;
import br.senai.sp.informatica.cadastro.model.Servico;
import br.senai.sp.informatica.cadastro.repository.ServicoRepository;

@Service
public class ServicoService {
	@Autowired
	private ServicoRepository repo;
	
	public List<Servico> getServicos() {
		return repo.findAll().stream()
				.filter(servico -> !servico.isDesativado())
				.collect(Collectors.toList());
	}
	
	public ListaDeServicos getListaDeServicos() {
		return new ListaDeServicos(getServicos(), null, -1);
	}
	
	public List<Servico> getServicos(Cliente cliente) {
		return repo.findAll().stream()
				.filter(servico -> !servico.isDesativado())
				.map(servico -> {
					servico.setSelecionado(cliente.getServicos().contains(servico));
					return servico;
				})
				.collect(Collectors.toList());
	}
	
	public void salvar(Servico servico) {
		repo.save(servico);
	}

	public boolean removeServico(int id) {
		Servico servico = getServico(id);
		if(servico != null) {
			repo.delete(servico);
			
			return true;
		} else {
			return false;
		}
	}
	
	public Servico getServico(int id) {
		return repo.findById(id).orElse(null);
	}
}
