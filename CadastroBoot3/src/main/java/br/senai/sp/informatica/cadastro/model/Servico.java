package br.senai.sp.informatica.cadastro.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;
import javax.validation.constraints.Size;

import lombok.Data;

@Data
@Entity
public class Servico {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idServico;
	@Size(min=3,max=150,message="O Nome deve ter no mínimo 3 e no máximo 150 caracteres")
	private String nome;
	private boolean desativado;
	@Transient
	private boolean selecionado;
}
