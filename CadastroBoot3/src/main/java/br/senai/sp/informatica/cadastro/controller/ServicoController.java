package br.senai.sp.informatica.cadastro.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import br.senai.sp.informatica.cadastro.model.Servico;
import br.senai.sp.informatica.cadastro.service.ServicoService;

@Controller
public class ServicoController {
	@Autowired
	private ServicoService dao;

	@RequestMapping({ "/editaServico" })
	public ModelAndView editaServico() {
		return new ModelAndView("editaServico", "servico", new Servico());
	}

	@RequestMapping("/salvaServico")
	public ModelAndView salvaServico(@ModelAttribute("servico") Servico servico, 
			RedirectAttributes redirectAttributes) {

		dao.salvar(servico);
		redirectAttributes.addFlashAttribute("mensagem", "Serviço cadastrado");
		redirectAttributes.addFlashAttribute("tipo.mensagem", "alert-success");
		return new ModelAndView("redirect:/editaServico");
	}

	@RequestMapping("/listaServico")
	public ModelAndView listaServico() {
		ModelAndView model = new ModelAndView("listaServico", "servicos", dao.getServicos());
		return model;
	}

	@RequestMapping("/removeServico/{idServico}")
	public String removeUsuario(@PathVariable("idServico") int id, Model model, 
			RedirectAttributes redirectAttributes) {

	    if (dao.removeServico(id)) {
			redirectAttributes.addFlashAttribute("mensagem", "Serviço excluído");
			redirectAttributes.addFlashAttribute("tipo.mensagem", "alert-success");
			return "redirect:/listaServico";
		} else {
			model.addAttribute("mensagem", "Houve falha ao excluir o Serviço");
			model.addAttribute("tipo.mensagem", "alert-danger");
			return "listaServico";
		}
	}
}
