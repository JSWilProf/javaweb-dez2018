package br.senai.sp.informatica.cadastro.service;

import java.util.Arrays;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.senai.sp.informatica.cadastro.model.Cliente;
import br.senai.sp.informatica.cadastro.model.ListaDeClientes;
import br.senai.sp.informatica.cadastro.repository.ClienteRepository;

@Service
public class ClienteService {
	@Autowired
	private ClienteRepository clienteRepo;
	
	public void salvar(Cliente cliente) {
		clienteRepo.save(cliente);
	}
	
	public ListaDeClientes getClientes() {
		return new ListaDeClientes(clienteRepo.findAll().stream()
				.filter(cliente -> !cliente.isDesativado())
				.collect(Collectors.toList())
				, null);
	}
	
	public boolean removeCliente(ListaDeClientes lista) {
		Arrays.stream(lista.getIds())
			.forEach(id -> {
				Cliente cliente = clienteRepo.findById(id).orElse(null);
				if(cliente != null) {
					cliente.setDesativado(true);
					clienteRepo.save(cliente);
				}
			});
		
		return true;
	}

	public Cliente getCliente(int id) {
		return clienteRepo.findById(id).orElse(null);
	}
}
