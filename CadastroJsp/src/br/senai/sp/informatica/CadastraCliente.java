package br.senai.sp.informatica;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@SuppressWarnings("serial")
@WebServlet("/CadastraCliente")
public class CadastraCliente extends HttpServlet {

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String nome = request.getParameter("nome");
		String endereco = request.getParameter("endereco");
		String telefone = request.getParameter("telefone");
		String email = request.getParameter("email");

		if(nome.isEmpty() || endereco.isEmpty() || 
		   telefone.isEmpty() || email.isEmpty()) { // se falta informação -> erro
			
			Mensagem msg = new Mensagem();
			msg.setTitulo("Erro");
			msg.setTexto("Alguns dados não foram informados");
			
			request.getSession().setAttribute("msg", Mensagem.builder()
					.titulo("Erro")
					.texto("Alguns dados não foram informados")
					.build());
			response.sendRedirect("erro.jsp");
		} else { // ok
			// Construindo um objeto para salvar as informações
			Cliente cliente = Cliente.builder()
					.nome(nome)
					.endereco(endereco)
					.telefone(telefone)
					.email(email)
					.build();
			
			// Salvar o objeto cliente na Sessão HTTP
			request.getSession().setAttribute("cliente", cliente);
			
			// Redirecionar para a página de finalização
			response.sendRedirect("listaCliente.jsp");
		}
	}
}
