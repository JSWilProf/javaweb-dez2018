package br.senai.sp.informatica.cadastro.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cliente {
	private Integer idCliente;
	private String nome;
	private String endereco;
	private String telefone;
	private String email;
	
	@Override
	public Cliente clone() {
		return Cliente.builder()
				.idCliente(new Integer(idCliente))
				.nome(new String(nome))
				.endereco(new String(endereco))
				.telefone(new String(telefone))
				.email(new String(email))
				.build();
	}
}
