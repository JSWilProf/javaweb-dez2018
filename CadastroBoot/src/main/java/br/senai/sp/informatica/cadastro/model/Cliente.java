package br.senai.sp.informatica.cadastro.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import br.senai.sp.informatica.cadastro.model.validacao.Logradouro;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Cliente {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer idCliente;
	@Size(min=3, max=150, message="O Nome deve ter no mínimo 3 e no máximo 150 caracteres")
	@Column(length=150) // Define para o Hibernate que o tamanho da coluna é de 150 caracters
	private String nome;
	@Logradouro(max=150, message="O Endereço é inválido")
	private String endereco;
	// http://www.anatel.gov.br/setorregulado/plano-de-numeracao-brasileiro
	@Pattern(regexp="(9[7-9][0-9]{3}|[2-5][0-9]{3})-[0-9]{4}", message="Nº de Telefone inválido")
	private String telefone;
	@Email(message="E-Mail inválido")
	private String email;
	private boolean desativado;
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinTable(name="ServicosPrestados",
		joinColumns={@JoinColumn(name="idCliente")},
		inverseJoinColumns={@JoinColumn(name="idServico")})
	private List<Servico> servicos;
}
